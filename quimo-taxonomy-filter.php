<?php

/**

    * Plugin Name: Quimo Taxonomy filter
    * Plugin URI:
    * Description: Aggiunge uno shortcode che consente di filtrare i post per tassonomia. RICHIEDE IL PLUGIN Quimo Field Mapper per mappare i dati estratti su un template.
    * Version: 1.0
    * Author: Simone Alati
    * Author URI: http://www.simonealati.it
*/

/*

████████╗ █████╗ ██╗  ██╗ ██████╗ ███╗   ██╗ ██████╗ ███╗   ███╗██╗   ██╗    ███████╗██╗██╗  ████████╗███████╗██████╗
╚══██╔══╝██╔══██╗╚██╗██╔╝██╔═══██╗████╗  ██║██╔═══██╗████╗ ████║╚██╗ ██╔╝    ██╔════╝██║██║  ╚══██╔══╝██╔════╝██╔══██╗
   ██║   ███████║ ╚███╔╝ ██║   ██║██╔██╗ ██║██║   ██║██╔████╔██║ ╚████╔╝     █████╗  ██║██║     ██║   █████╗  ██████╔╝
   ██║   ██╔══██║ ██╔██╗ ██║   ██║██║╚██╗██║██║   ██║██║╚██╔╝██║  ╚██╔╝      ██╔══╝  ██║██║     ██║   ██╔══╝  ██╔══██╗
   ██║   ██║  ██║██╔╝ ██╗╚██████╔╝██║ ╚████║╚██████╔╝██║ ╚═╝ ██║   ██║       ██║     ██║███████╗██║   ███████╗██║  ██║
   ╚═╝   ╚═╝  ╚═╝╚═╝  ╚═╝ ╚═════╝ ╚═╝  ╚═══╝ ╚═════╝ ╚═╝     ╚═╝   ╚═╝       ╚═╝     ╚═╝╚══════╝╚═╝   ╚══════╝╚═╝  ╚═╝

http://patorjk.com/software/taag/#p=display&f=ANSI%20Shadow&t=Taxonomy%20filter

ESEMPIO D'USO

[txf taxonomy="genre|product_cat" term="donna,bambino|mocassini,snickers" template="test"]
[txf taxonomy="product_tag" term="ufficio" template="test" debug="1"]

Recupera i prodotti di categoria mocassini e snickers per donna e per bambino (tassonomia custom genre). I dati recuperati sono mappati sul template plugins/field-mapper/templates/test.html

*/

class taxonomyFilter {

    public function wpQueryArgsComposer($taxonomies, $terms, $debug) {
        if (strpos($taxonomies, '|') !== false && strpos($terms, '|') !== false) {
            /* ho valori multipli per tassomonia (e termini) */
            $taxonomies = explode('|', $taxonomies);
            $terms = explode('|', $terms);
            if (count($taxonomies) != count($terms)) return false;
            /* inserisco i vincoli tassonomia = termine/i */
            $tax_query = array('relation' => 'AND');
            for ($i = 0; $i < count($taxonomies); $i++) {
                $tax_query[] = array(
                    'taxonomy'	=> $taxonomies[$i],
            		'field' 	=> 'slug',
            		'terms'    	=> $this->explodeTerms($terms[$i])
                );
            }
            $args = array(
                //'post_type' => 'product',
                'tax_query' => $tax_query,
            );
			if ($debug) $this->debug($args);
            return $args;
        } elseif (strpos($taxonomies, '|') === false && strpos($terms, '|') === false) {
            /* ho una sola tassomonia (e termine) */
            $args = array(
                //'post_type' => 'product',
                'tax_query' => array(
                	array(
                		'taxonomy'	=> $taxonomies,
                		'field' 	=> 'slug',
                		'terms'    	=> $this->explodeTerms($terms)
                	),
                )
            );
			if ($debug) $this->debug($args);
            return $args;
        } else {
            /* tassonomie e termini non corrispondono */
			if ($debug) echo "Tassonomie e termini non corrispondono";
            return false;
        }
    }

    private function explodeTerms($term) {
        $terms = (strpos($term, ',') !== false) ? explode(',', $term) : $term;
        return $terms;
    }
	
	private function debug($args) {
		echo "ARGOMENTI PASSATI A WP_Query<br>";
		var_dump($args);
		echo "<br><br>";
	}

}

add_action('init', function() {
    add_shortcode('txf', 'taxonomyFilterShortcode');
});

function taxonomyFilterShortcode($atts, $content = null) {
    extract(shortcode_atts(array(
        'taxonomy' => 'product_cat',
        'term' => '',
        'template' => 'test',
		'debug' => 0
    ),
    $atts, 'txf'));
    if (!$term) return false;
    $txf = new taxonomyFilter();
    $args = $txf->wpQueryArgsComposer($taxonomy, $term, $debug);
    if (class_exists('fieldMapper')) {
        $fm = new fieldMapper();
        $fm->get($args,$debug);
        return $fm->render($template, $debug);
    } else {
        var_dump($args);
        return "Bisogna installare ed attivare il plugin field-mapper!";
    }
}
